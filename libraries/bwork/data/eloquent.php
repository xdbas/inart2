<?php
/**
 * Bwork Framework
 *
 * @package Bwork
 * @subpackage Bwork_Data
 * @author Bas van Manen <basje1[at]gmail.com>
 * @version $id: Bwork Framework v 0.1
 * @license http://creativecommons.org/licenses/by-nc-sa/3.0/
 */

/**
 * PDO
 *
 * This class holds an connection with a MySQL Database via PDO
 *
 * @abstract
 * @package Bwork
 * @subpackage Bwork_Data
 * @version v 0.1
 */
abstract class Bwork_Data_Eloquent extends Illuminate\Database\Eloquent\Model implements Bwork_Data_Interface
{

    public static $capsule;

    /**
     * This will hold the data object
     *
     * @var  $db
     * @access protected
     */
    protected $db;

    /**
     * The construction method will attempt to establish a connection with the
     * MySQL database and stores itself in $db.
     *
     * @access public
     * @return \Bwork_Data_Eloquent
     */
    public function  __construct()
    {
        try {
            static::capsule();
        } catch(Exception $e) {

        }
    }

    private static function capsule()
    {
        if (false ===  static::$capsule instanceof Capsule) {
            $dbParams   = Bwork_Core_Registry::GetInstance()
                ->getResource('Bwork_Config_Confighandler')
                ->get('database');

            static::$capsule = new Bwork_Data_Eloquent_Capsule(array(
                'driver'    => 'mysql',
                'host'      => $dbParams['host'],
                'database'  => $dbParams['dbname'],
                'username'  => $dbParams['username'],
                'password'  => $dbParams['password'],
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
            ));
        }

        return static::$capsule;
    }


    /**
     * Magic method automatically called just before destroying the object and
     * will unset PDO
     *
     * @access public
     * @return void
     */
    public function  __destruct()
    {
        unset($this->db);
    }

    /**
     * The main function that will return the PDO class
     *
     * @access public
     * @return PDO
     */
    public function db()
    {
        return static::capsule();
    }

}