<?php
use \Illuminate\Database\Capsule\Manager as Manager;
use \Illuminate\Events\Dispatcher;
use \Illuminate\Container\Container;

class Bwork_Data_Eloquent_Capsule extends Manager {

    public function __construct(array $options = array())
    {
        parent::__construct();

        $this->addConnection($options);

        $this->setEventDispatcher(new Dispatcher(new Container));
        $this->setAsGlobal();
        $this->bootEloquent();
    }

}