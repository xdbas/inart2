<?php
/**
 * Bwork Framework
 *
 * @package Bwork
 * @subpackage Bwork_Helper
 * @author Bas van Manen <basje1[at]gmail.com>
 * @version $id: Bwork Framework v 0.1
 * @license http://creativecommons.org/licenses/by-nc-sa/3.0/
 */

/**
 * Asset Helper
 *
 * Asset helper can be called from a view or helper and can be used
 * as the following:
 *
 * Example:
 * <code>
 *  <?php echo $this->baseUrl(string $url, boolean $ssl); ?>
 * </code>
 *
 * @package Bwork
 * @subpackage Bwork_Helper
 * @version v 0.2
 */
class Bwork_Helper_Asset
{

    private static $jsAssets = array();

    private static $cssAssets = array();

    private static $which = 'js';

    /**
     * @param String|Array|Traversable $location
     */
    public static function addJS($location)
    {
        $helper = Bwork_Core_Registry::getInstance()->getResource('Bwork_Http_Request');

        if (is_array($location)
            || $location instanceof Traversable
        ) {
            foreach ($location as $loc) {
                if (in_array($loc, static::$jsAssets) === false) {
                    static::$jsAssets[] =  $helper->create($loc, false, false);;
                }

                continue;
            }
        } else {
            if (in_array($location, static::$jsAssets) === false) {
                static::$jsAssets[] = $helper->create($location, false, false);;
            }
        }
    }

    /**
     * @param String|Array|Traversable $location
     */
    public static function addCSS($location)
    {
        $helper = Bwork_Core_Registry::getInstance()->getResource('Bwork_Http_Request');

        if (is_array($location)
            || $location instanceof Traversable
        ) {
            foreach ($location as $loc) {
                if (in_array($loc, static::$cssAssets) === false) {
                    static::$cssAssets[] = $helper->create($loc, false, false);;
                }

                continue;
            }
        } else {
            if (in_array($location, static::$cssAssets) === false) {
                static::$cssAssets[] = $helper->create($location, false, false);;
            }
        }
    }

    public static function asset($which)
    {
        switch ($which) {
            case 'js':
                static::$which = 'js';
                break;
            case 'css':
                static::$which = 'css';
                break;
        }

        return new static;
    }

    public function __toString()
    {
        switch (static::$which) {
            case 'js':
                return static::locationToHTML('js');
            case 'css':
                return static::locationToHTML('css');
        }
    }

    public static function locationToHTML($type) {
        if ($type == 'js') {
            $jsHTML = '';
            foreach (static::$jsAssets as $js) {
                $jsHTML .= '<script type="text/javascript" src="' . $js . '"></script>';
            }

            return $jsHTML;
        }

        if ($type == 'css') {
            $cssHTML = '';
            foreach (static::$cssAssets as $css) {
                $cssHTML .=  '<link href="' . $css . '" rel="stylesheet" />';
            }

            return $cssHTML;
        }
    }

}