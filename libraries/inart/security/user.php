<?php

class Inart_Security_User {

    private $hash;
    private $userModel;

    public function __construct() {

        $this->userModel = new AuthUserModel();
        $this->hash = hash(
            'sha256',
            sha1($_SERVER['REMOTE_ADDR'].(array_key_exists('HTTP_USER_AGENT', $_SERVER) === false)
                    ? ''
                    : $_SERVER['HTTP_USER_AGENT']
            )
        );

        if(isset($_SESSION['loginUsername'])) {

            if($this->isLoggedIn() == false) {
                $this->logOutUser();
            }
        }
    }

    private function checkLogin($username, $password) {
        $password = hash("sha256", $password);

        return $this->userModel->checkUserLogin($username, $password);
    }

    public function loginUser($username, $password) {
        if($this->isLoggedIn() == false && $this->checkLogin($username, $password) == true) {
            $_SESSION['loginUsername'] = $username;
            $_SESSION['loginHash'] = $this->hash;

            return $this->updateHash($username);
        }

        return false;
    }

    private function updateHash($username) {
        return $this->userModel->updateHash($username, $this->hash);
    }

    private function compareHash($username, $hash) {
        return $this->userModel->getUserByHash($username, $hash);
    }

    public function isLoggedIn() {

        if(isset($_SESSION['loginUsername']) && isset($_SESSION['loginHash'])) {
            return $this->compareHash($_SESSION['loginUsername'], $_SESSION['loginHash']);
        }

        return false;
    }

    public function logOutUser() {
        unset($_SESSION['loginUsername'], $_SESSION['loginHash']);

        return $this->isLoggedIn() == false;
    }

    public function getUserDataByUsername($username) {
        return $this->userModel->getUserDataByUsername($username);
    }

    public function getCurrentUserData()
    {
        return $this->getUserDataByUsername($_SESSION['loginUsername']);
    }
}