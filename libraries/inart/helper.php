<?php

class Inart_Helper
{
    public static function redirect($url) {
        header("Location: " . $url);
        exit;
    }
}