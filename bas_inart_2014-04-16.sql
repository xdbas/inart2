# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.33)
# Database: bas_inart
# Generation Time: 2014-04-16 13:05:23 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table artists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `artists`;

CREATE TABLE `artists` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `create_timestamp` int(11) unsigned DEFAULT NULL,
  `update_timestamp` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `artists_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `artists` WRITE;
/*!40000 ALTER TABLE `artists` DISABLE KEYS */;

INSERT INTO `artists` (`id`, `user_id`, `name`, `image`, `url`, `description`, `create_timestamp`, `update_timestamp`)
VALUES
	(2,1,'Bas','http://dummyimage.com/200X300/aae698/ffffff.png','bas','Phasellus mollis porttitor hendrerit. Etiam vitae sem ante. Vivamus fringilla scelerisque tellus, non tincidunt dolor molestie ac. Vestibulum euismod dapibus libero, non lobortis odio commodo eu. Etiam urna ante, lobortis et bibendum malesuada, convallis non lacus. Quisque placerat sodales convallis. Sed bibendum et libero vitae aliquet. Nullam posuere iaculis justo, sit amet accumsan quam tristique a. Praesent vel tincidunt tortor. Morbi id diam blandit, congue nunc sed, vehicula elit. Ut vitae pulvinar est. Proin vitae purus eu massa suscipit facilisis. Morbi rutrum libero erat, nec accumsan risus vehicula quis. Aenean sed mi convallis, venenatis lectus nec, tempus tortor. Morbi volutpat eros est, a volutpat elit dictum ac. Desc.......',NULL,NULL),
	(3,4,'Artist1','http://dummyimage.com/200X400/aae698/ffffff.png','artist1','Phasellus mollis porttitor hendrerit. Etiam vitae sem ante. Vivamus fringilla scelerisque tellus, non tincidunt dolor molestie ac. Vestibulum euismod dapibus libero, non lobortis odio commodo eu. Etiam urna ante, lobortis et bibendum malesuada, convallis non lacus. Quisque placerat sodales convallis. Sed bibendum et libero vitae aliquet. Nullam posuere iaculis justo, sit amet accumsan quam tristique a. Praesent vel tincidunt tortor. Morbi id diam blandit, congue nunc sed, vehicula elit. Ut vitae pulvinar est. Proin vitae purus eu massa suscipit facilisis. Morbi rutrum libero erat, nec accumsan risus vehicula quis. Aenean sed mi convallis, venenatis lectus nec, tempus tortor. Morbi volutpat eros est, a volutpat elit dictum ac. Description.........',NULL,NULL),
	(4,5,'Artist2','http://dummyimage.com/200X300/aae698/ffffff.png','artist2','Phasellus mollis porttitor hendrerit. Etiam vitae sem ante. Vivamus fringilla scelerisque tellus, non tincidunt dolor molestie ac. Vestibulum euismod dapibus libero, non lobortis odio commodo eu. Etiam urna ante, lobortis et bibendum malesuada, convallis non lacus. Quisque placerat sodales convallis. Sed bibendum et libero vitae aliquet. Nullam posuere iaculis justo, sit amet accumsan quam tristique a. Praesent vel tincidunt tortor. Morbi id diam blandit, congue nunc sed, vehicula elit. Ut vitae pulvinar est. Proin vitae purus eu massa suscipit facilisis. Morbi rutrum libero erat, nec accumsan risus vehicula quis. Aenean sed mi convallis, venenatis lectus nec, tempus tortor. Morbi volutpat eros est, a volutpat elit dictum ac. Hallooo\n',NULL,NULL),
	(5,6,'Artist3','http://dummyimage.com/200X250/aae698/ffffff.png','artist3','Phasellus mollis porttitor hendrerit. Etiam vitae sem ante. Vivamus fringilla scelerisque tellus, non tincidunt dolor molestie ac. Vestibulum euismod dapibus libero, non lobortis odio commodo eu. Etiam urna ante, lobortis et bibendum malesuada, convallis non lacus. Quisque placerat sodales convallis. Sed bibendum et libero vitae aliquet. Nullam posuere iaculis justo, sit amet accumsan quam tristique a. Praesent vel tincidunt tortor. Morbi id diam blandit, congue nunc sed, vehicula elit. Ut vitae pulvinar est. Proin vitae purus eu massa suscipit facilisis. Morbi rutrum libero erat, nec accumsan risus vehicula quis. Aenean sed mi convallis, venenatis lectus nec, tempus tortor. Morbi volutpat eros est, a volutpat elit dictum ac. Heyyy',NULL,NULL),
	(6,7,'Artist4','http://dummyimage.com/230X300/aae698/ffffff.png','artist4','Phasellus mollis porttitor hendrerit. Etiam vitae sem ante. Vivamus fringilla scelerisque tellus, non tincidunt dolor molestie ac. Vestibulum euismod dapibus libero, non lobortis odio commodo eu. Etiam urna ante, lobortis et bibendum malesuada, convallis non lacus. Quisque placerat sodales convallis. Sed bibendum et libero vitae aliquet. Nullam posuere iaculis justo, sit amet accumsan quam tristique a. Praesent vel tincidunt tortor. Morbi id diam blandit, congue nunc sed, vehicula elit. Ut vitae pulvinar est. Proin vitae purus eu massa suscipit facilisis. Morbi rutrum libero erat, nec accumsan risus vehicula quis. Aenean sed mi convallis, venenatis lectus nec, tempus tortor. Morbi volutpat eros est, a volutpat elit dictum ac. HALLO :D',NULL,NULL);

/*!40000 ALTER TABLE `artists` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table auctions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auctions`;

CREATE TABLE `auctions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `artist_id` int(11) unsigned DEFAULT NULL,
  `create_timestamp` int(11) unsigned DEFAULT NULL,
  `update_timestamp` int(11) unsigned DEFAULT NULL,
  `minimal_bid` int(11) unsigned DEFAULT NULL,
  `timer` int(11) unsigned DEFAULT NULL,
  `state` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `artist_id` (`artist_id`),
  CONSTRAINT `auctions_ibfk_2` FOREIGN KEY (`artist_id`) REFERENCES `artists` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `auctions` WRITE;
/*!40000 ALTER TABLE `auctions` DISABLE KEYS */;

INSERT INTO `auctions` (`id`, `artist_id`, `create_timestamp`, `update_timestamp`, `minimal_bid`, `timer`, `state`)
VALUES
	(2,2,NULL,NULL,100,1397743153,1),
	(3,2,NULL,NULL,50,1397743153,1),
	(4,2,NULL,NULL,500,1397743153,1),
	(5,2,NULL,NULL,133,1397743153,1);

/*!40000 ALTER TABLE `auctions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bids
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bids`;

CREATE TABLE `bids` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `auction_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `amount` int(10) unsigned NOT NULL,
  `updated_at` varchar(255) DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `auction_id` (`auction_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `bids_ibfk_1` FOREIGN KEY (`auction_id`) REFERENCES `auctions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `bids_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `bids` WRITE;
/*!40000 ALTER TABLE `bids` DISABLE KEYS */;

INSERT INTO `bids` (`id`, `auction_id`, `user_id`, `amount`, `updated_at`, `created_at`)
VALUES
	(12,2,2,518,'2014-04-16 14:46:39','2014-04-16 14:46:39'),
	(13,2,2,500,'2014-04-16 14:49:05','2014-04-16 14:49:05'),
	(14,2,2,100,'2014-04-16 14:49:08','2014-04-16 14:49:08'),
	(15,2,2,100,'2014-04-16 14:50:50','2014-04-16 14:50:50'),
	(16,2,2,520,'2014-04-16 14:51:13','2014-04-16 14:51:13'),
	(17,2,2,530,'2014-04-16 14:51:47','2014-04-16 14:51:47'),
	(18,2,2,533,'2014-04-16 14:52:20','2014-04-16 14:52:20'),
	(19,2,2,544,'2014-04-16 14:52:31','2014-04-16 14:52:31'),
	(20,2,2,560,'2014-04-16 14:55:07','2014-04-16 14:55:07'),
	(21,2,2,566,'2014-04-16 14:55:15','2014-04-16 14:55:15');

/*!40000 ALTER TABLE `bids` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table buyers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `buyers`;

CREATE TABLE `buyers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `buyers_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `buyers` WRITE;
/*!40000 ALTER TABLE `buyers` DISABLE KEYS */;

INSERT INTO `buyers` (`id`, `user_id`)
VALUES
	(1,1);

/*!40000 ALTER TABLE `buyers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table catories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `catories`;

CREATE TABLE `catories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_unique` (`name`),
  UNIQUE KEY `url_unique` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `catories` WRITE;
/*!40000 ALTER TABLE `catories` DISABLE KEYS */;

INSERT INTO `catories` (`id`, `name`, `url`)
VALUES
	(1,'Abstract','abstract');

/*!40000 ALTER TABLE `catories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_images`;

CREATE TABLE `product_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `product_images_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `product_images` WRITE;
/*!40000 ALTER TABLE `product_images` DISABLE KEYS */;

INSERT INTO `product_images` (`id`, `product_id`, `url`, `title`)
VALUES
	(1,2,'http://dummyimage.com/500X500/aae698/ffffff.png','LOL'),
	(2,2,'http://dummyimage.com/500X500/aae698/ffffff.png','LOL2'),
	(3,3,'http://dummyimage.com/500X500/aae698/ffffff.png',NULL),
	(4,4,'http://dummyimage.com/500X500/aae698/ffffff.png',NULL),
	(5,5,'http://dummyimage.com/500X500/aae698/ffffff.png',NULL);

/*!40000 ALTER TABLE `product_images` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `artist_id` int(11) unsigned DEFAULT NULL,
  `category_id` int(11) unsigned DEFAULT NULL,
  `create_timestamp` int(11) unsigned DEFAULT NULL,
  `update_timestamp` int(11) unsigned DEFAULT NULL,
  `information` text,
  `auction_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `artist_id` (`artist_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `products_ibfk_1` FOREIGN KEY (`artist_id`) REFERENCES `artists` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `products_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `catories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `name`, `artist_id`, `category_id`, `create_timestamp`, `update_timestamp`, `information`, `auction_id`)
VALUES
	(2,'Artwork1',2,1,0,0,'Phasellus mollis porttitor hendrerit. Etiam vitae sem ante. Vivamus fringilla scelerisque tellus, non tincidunt dolor molestie ac. Vestibulum euismod dapibus libero, non lobortis odio commodo eu. Etiam urna ante, lobortis et bibendum malesuada, convallis non lacus. Quisque placerat sodales convallis. Sed bibendum et libero vitae aliquet. Nullam posuere iaculis justo, sit amet accumsan quam tristique a. Praesent vel tincidunt tortor. Morbi id diam blandit, congue nunc sed, vehicula elit. Ut vitae pulvinar est. Proin vitae purus eu massa suscipit facilisis. Morbi rutrum libero erat, nec accumsan risus vehicula quis. Aenean sed mi convallis, venenatis lectus nec, tempus tortor. Morbi volutpat eros est, a volutpat elit dictum ac. ',2),
	(3,'Artwork2',2,1,0,0,'Phasellus mollis porttitor hendrerit. Etiam vitae sem ante. Vivamus fringilla scelerisque tellus, non tincidunt dolor molestie ac. Vestibulum euismod dapibus libero, non lobortis odio commodo eu. Etiam urna ante, lobortis et bibendum malesuada, convallis non lacus. Quisque placerat sodales convallis. Sed bibendum et libero vitae aliquet. Nullam posuere iaculis justo, sit amet accumsan quam tristique a. Praesent vel tincidunt tortor. Morbi id diam blandit, congue nunc sed, vehicula elit. Ut vitae pulvinar est. Proin vitae purus eu massa suscipit facilisis. Morbi rutrum libero erat, nec accumsan risus vehicula quis. Aenean sed mi convallis, venenatis lectus nec, tempus tortor. Morbi volutpat eros est, a volutpat elit dictum ac. ',3),
	(4,'Artwork3',2,1,0,0,'Phasellus mollis porttitor hendrerit. Etiam vitae sem ante. Vivamus fringilla scelerisque tellus, non tincidunt dolor molestie ac. Vestibulum euismod dapibus libero, non lobortis odio commodo eu. Etiam urna ante, lobortis et bibendum malesuada, convallis non lacus. Quisque placerat sodales convallis. Sed bibendum et libero vitae aliquet. Nullam posuere iaculis justo, sit amet accumsan quam tristique a. Praesent vel tincidunt tortor. Morbi id diam blandit, congue nunc sed, vehicula elit. Ut vitae pulvinar est. Proin vitae purus eu massa suscipit facilisis. Morbi rutrum libero erat, nec accumsan risus vehicula quis. Aenean sed mi convallis, venenatis lectus nec, tempus tortor. Morbi volutpat eros est, a volutpat elit dictum ac. ',4),
	(5,'Artwork4',2,1,0,0,'Phasellus mollis porttitor hendrerit. Etiam vitae sem ante. Vivamus fringilla scelerisque tellus, non tincidunt dolor molestie ac. Vestibulum euismod dapibus libero, non lobortis odio commodo eu. Etiam urna ante, lobortis et bibendum malesuada, convallis non lacus. Quisque placerat sodales convallis. Sed bibendum et libero vitae aliquet. Nullam posuere iaculis justo, sit amet accumsan quam tristique a. Praesent vel tincidunt tortor. Morbi id diam blandit, congue nunc sed, vehicula elit. Ut vitae pulvinar est. Proin vitae purus eu massa suscipit facilisis. Morbi rutrum libero erat, nec accumsan risus vehicula quis. Aenean sed mi convallis, venenatis lectus nec, tempus tortor. Morbi volutpat eros est, a volutpat elit dictum ac. ',5);

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `hash` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `password`, `hash`, `name`, `avatar`, `create_time`, `update_time`)
VALUES
	(1,'','henkje',NULL,'Bas van Manen',NULL,NULL,NULL),
	(2,'','pokemon',NULL,'Jannick den hoedt',NULL,NULL,NULL),
	(3,'iris','47612b3175fece07f6c3e91992412c5b16ca88a9068cb72fecbcf653eb5ffcd7','07e23ede2756aa3f5f7cc9759117c4910875e032c27b8556a1e20626224f10ec','Iris',NULL,NULL,NULL),
	(4,'artist1','47612b3175fece07f6c3e91992412c5b16ca88a9068cb72fecbcf653eb5ffcd7',NULL,'',NULL,NULL,NULL),
	(5,'artist2','47612b3175fece07f6c3e91992412c5b16ca88a9068cb72fecbcf653eb5ffcd7',NULL,'',NULL,NULL,NULL),
	(6,'artist3','47612b3175fece07f6c3e91992412c5b16ca88a9068cb72fecbcf653eb5ffcd7',NULL,'',NULL,NULL,NULL),
	(7,'artist4','47612b3175fece07f6c3e91992412c5b16ca88a9068cb72fecbcf653eb5ffcd7',NULL,'',NULL,NULL,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
