-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Machine: localhost
-- Genereertijd: 16 apr 2014 om 14:44
-- Serverversie: 5.5.16
-- PHP-Versie: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bas_inart`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `artists`
--

CREATE TABLE IF NOT EXISTS `artists` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `create_timestamp` int(11) unsigned DEFAULT NULL,
  `update_timestamp` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Gegevens worden uitgevoerd voor tabel `artists`
--

INSERT INTO `artists` (`id`, `user_id`, `name`, `image`, `url`, `description`, `create_timestamp`, `update_timestamp`) VALUES
(2, 1, 'Bas', 'http://dummyimage.com/200X300/aae698/ffffff.png', 'bas', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque euismod lacus eget ipsum consequat mattis. Nam luctus metus purus, eu aliquam enim aliquam scelerisque. Nunc placerat magna eu turpis pulvinar, eu porta leo congue. Nam rutrum dapibus molestie. Curabitur id mauris ac ipsum ultricies placerat. Curabitur facilisis elementum lacus, ac consequat mi facilisis eget. Nulla facilisi. Nullam eleifend porttitor risus eget vestibulum.', NULL, NULL),
(3, 4, 'Artist1', 'http://dummyimage.com/200X400/aae698/ffffff.png', 'artist1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque euismod lacus eget ipsum consequat mattis. Nam luctus metus purus, eu aliquam enim aliquam scelerisque. Nunc placerat magna eu turpis pulvinar, eu porta leo congue. Nam rutrum dapibus molestie. Curabitur id mauris ac ipsum ultricies placerat. Curabitur facilisis elementum lacus, ac consequat mi facilisis eget. Nulla facilisi. Nullam eleifend porttitor risus eget vestibulum.', NULL, NULL),
(4, 5, 'Artist2', 'http://dummyimage.com/200X300/aae698/ffffff.png', 'artist2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque euismod lacus eget ipsum consequat mattis. Nam luctus metus purus, eu aliquam enim aliquam scelerisque. Nunc placerat magna eu turpis pulvinar, eu porta leo congue. Nam rutrum dapibus molestie. Curabitur id mauris ac ipsum ultricies placerat. Curabitur facilisis elementum lacus, ac consequat mi facilisis eget. Nulla facilisi. Nullam eleifend porttitor risus eget vestibulum.', NULL, NULL),
(5, 6, 'Artist3', 'http://dummyimage.com/200X250/aae698/ffffff.png', 'artist3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque euismod lacus eget ipsum consequat mattis. Nam luctus metus purus, eu aliquam enim aliquam scelerisque. Nunc placerat magna eu turpis pulvinar, eu porta leo congue. Nam rutrum dapibus molestie. Curabitur id mauris ac ipsum ultricies placerat. Curabitur facilisis elementum lacus, ac consequat mi facilisis eget. Nulla facilisi. Nullam eleifend porttitor risus eget vestibulum.', NULL, NULL),
(6, 7, 'Artist4', 'http://dummyimage.com/230X300/aae698/ffffff.png', 'artist4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque euismod lacus eget ipsum consequat mattis. Nam luctus metus purus, eu aliquam enim aliquam scelerisque. Nunc placerat magna eu turpis pulvinar, eu porta leo congue. Nam rutrum dapibus molestie. Curabitur id mauris ac ipsum ultricies placerat. Curabitur facilisis elementum lacus, ac consequat mi facilisis eget. Nulla facilisi. Nullam eleifend porttitor risus eget vestibulum.', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `auctions`
--

CREATE TABLE IF NOT EXISTS `auctions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `artist_id` int(11) unsigned DEFAULT NULL,
  `create_timestamp` int(11) unsigned DEFAULT NULL,
  `update_timestamp` int(11) unsigned DEFAULT NULL,
  `minimal_bid` int(11) unsigned DEFAULT NULL,
  `timer` int(11) unsigned DEFAULT NULL,
  `state` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `artist_id` (`artist_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Gegevens worden uitgevoerd voor tabel `auctions`
--

INSERT INTO `auctions` (`id`, `artist_id`, `create_timestamp`, `update_timestamp`, `minimal_bid`, `timer`, `state`) VALUES
(2, 2, NULL, NULL, 100, 1397743153, 1),
(3, 2, NULL, NULL, 50, 1397743153, 1),
(4, 2, NULL, NULL, 500, 1397743153, 1),
(5, 2, NULL, NULL, 133, 1397743153, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `bids`
--

CREATE TABLE IF NOT EXISTS `bids` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `auction_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `amount` int(10) unsigned NOT NULL,
  `updated_at` int(10) unsigned DEFAULT NULL,
  `created_at` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `auction_id` (`auction_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Gegevens worden uitgevoerd voor tabel `bids`
--

INSERT INTO `bids` (`id`, `auction_id`, `user_id`, `amount`, `updated_at`, `created_at`) VALUES
(1, 2, 2, 100, NULL, NULL),
(2, 2, 2, 150, NULL, NULL),
(3, 2, 2, 155, 2014, 2014),
(4, 2, 2, 160, 2014, 2014),
(5, 2, 2, 177, 2014, 2014),
(6, 4, 2, 0, 2014, 2014),
(7, 4, 2, 8, 2014, 2014),
(8, 4, 2, 9, 2014, 2014),
(9, 4, 2, 9, 2014, 2014),
(10, 4, 2, 10000, 2014, 2014),
(11, 4, 2, 0, 2014, 2014),
(12, 4, 2, 10, 2014, 2014),
(13, 4, 2, 5, 2014, 2014),
(14, 5, 2, 5, 2014, 2014),
(15, 2, 2, 100, 2014, 2014),
(16, 2, 2, 190, 2014, 2014);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `buyers`
--

CREATE TABLE IF NOT EXISTS `buyers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Gegevens worden uitgevoerd voor tabel `buyers`
--

INSERT INTO `buyers` (`id`, `user_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `catories`
--

CREATE TABLE IF NOT EXISTS `catories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_unique` (`name`),
  UNIQUE KEY `url_unique` (`url`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Gegevens worden uitgevoerd voor tabel `catories`
--

INSERT INTO `catories` (`id`, `name`, `url`) VALUES
(1, 'Abstract', 'abstract');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `artist_id` int(11) unsigned DEFAULT NULL,
  `category_id` int(11) unsigned DEFAULT NULL,
  `create_timestamp` int(11) unsigned DEFAULT NULL,
  `update_timestamp` int(11) unsigned DEFAULT NULL,
  `information` text,
  `auction_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `artist_id` (`artist_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Gegevens worden uitgevoerd voor tabel `products`
--

INSERT INTO `products` (`id`, `name`, `artist_id`, `category_id`, `create_timestamp`, `update_timestamp`, `information`, `auction_id`) VALUES
(2, 'Artwork1', 2, 1, 0, 0, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec dui dignissim, pellentesque eros sed, sagittis elit. Donec lobortis rutrum diam, ut tincidunt est volutpat et. Proin sed blandit augue, id facilisis erat. Donec vulputate urna quis nibh tincidunt, non pretium sapien malesuada. Proin tempor condimentum ante quis tincidunt. Nullam adipiscing quis velit sed facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 2),
(3, 'Artwork2', 2, 1, 0, 0, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec dui dignissim, pellentesque eros sed, sagittis elit. Donec lobortis rutrum diam, ut tincidunt est volutpat et. Proin sed blandit augue, id facilisis erat. Donec vulputate urna quis nibh tincidunt, non pretium sapien malesuada. Proin tempor condimentum ante quis tincidunt. Nullam adipiscing quis velit sed facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 3),
(4, 'Artwork3', 2, 1, 0, 0, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec dui dignissim, pellentesque eros sed, sagittis elit. Donec lobortis rutrum diam, ut tincidunt est volutpat et. Proin sed blandit augue, id facilisis erat. Donec vulputate urna quis nibh tincidunt, non pretium sapien malesuada. Proin tempor condimentum ante quis tincidunt. Nullam adipiscing quis velit sed facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 4),
(5, 'Artwork4', 2, 1, 0, 0, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec dui dignissim, pellentesque eros sed, sagittis elit. Donec lobortis rutrum diam, ut tincidunt est volutpat et. Proin sed blandit augue, id facilisis erat. Donec vulputate urna quis nibh tincidunt, non pretium sapien malesuada. Proin tempor condimentum ante quis tincidunt. Nullam adipiscing quis velit sed facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 5);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `product_images`
--

CREATE TABLE IF NOT EXISTS `product_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Gegevens worden uitgevoerd voor tabel `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `url`, `title`) VALUES
(1, 2, 'http://dummyimage.com/500X500/aae698/ffffff.png', 'LOL'),
(2, 2, 'http://dummyimage.com/500X500/aae698/ffffff.png', 'LOL2'),
(3, 3, 'http://dummyimage.com/500X500/aae698/ffffff.png', NULL),
(4, 4, 'http://dummyimage.com/500X500/aae698/ffffff.png', NULL),
(5, 5, 'http://dummyimage.com/500X500/aae698/ffffff.png', NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `hash` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Gegevens worden uitgevoerd voor tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `hash`, `name`, `avatar`, `create_time`, `update_time`) VALUES
(1, '', 'henkje', NULL, 'Bas van Manen', NULL, NULL, NULL),
(2, '', 'pokemon', NULL, 'Jannick den hoedt', NULL, NULL, NULL),
(3, 'iris', '47612b3175fece07f6c3e91992412c5b16ca88a9068cb72fecbcf653eb5ffcd7', '07e23ede2756aa3f5f7cc9759117c4910875e032c27b8556a1e20626224f10ec', 'Iris', NULL, NULL, NULL),
(4, 'artist1', '47612b3175fece07f6c3e91992412c5b16ca88a9068cb72fecbcf653eb5ffcd7', NULL, '', NULL, NULL, NULL),
(5, 'artist2', '47612b3175fece07f6c3e91992412c5b16ca88a9068cb72fecbcf653eb5ffcd7', NULL, '', NULL, NULL, NULL),
(6, 'artist3', '47612b3175fece07f6c3e91992412c5b16ca88a9068cb72fecbcf653eb5ffcd7', NULL, '', NULL, NULL, NULL),
(7, 'artist4', '47612b3175fece07f6c3e91992412c5b16ca88a9068cb72fecbcf653eb5ffcd7', NULL, '', NULL, NULL, NULL);

--
-- Beperkingen voor gedumpte tabellen
--

--
-- Beperkingen voor tabel `artists`
--
ALTER TABLE `artists`
  ADD CONSTRAINT `artists_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `auctions`
--
ALTER TABLE `auctions`
  ADD CONSTRAINT `auctions_ibfk_2` FOREIGN KEY (`artist_id`) REFERENCES `artists` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `bids`
--
ALTER TABLE `bids`
  ADD CONSTRAINT `bids_ibfk_1` FOREIGN KEY (`auction_id`) REFERENCES `auctions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bids_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `buyers`
--
ALTER TABLE `buyers`
  ADD CONSTRAINT `buyers_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`artist_id`) REFERENCES `artists` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `catories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
