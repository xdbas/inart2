<?php
class AuthUserModel extends Bwork_Data_PDO {

    public function getUserByHash($username, $hash) {
        $stmt = $this->db()->prepare("SELECT * FROM `users` WHERE `username` = ? AND `hash` = ?");
        $stmt->bindParam(1, $username, PDO::PARAM_STR, 50);
        $stmt->bindParam(2, $hash, PDO::PARAM_STR, 255);
        $stmt->execute();

        return $stmt->rowCount() > 0;
    }

    public function getUserDataByUsername($username) {
        $stmt = $this->db()->prepare("SELECT * FROM `users` WHERE `username` = ?");
        $stmt->bindParam(1, $username, PDO::PARAM_STR, 50);
        $stmt->execute();

        return $stmt->fetchObject("UserVO");
    }

    public function checkUserLogin($username, $password) {
        $stmt = $this->db()->prepare("SELECT * FROM `users` WHERE `username` = ? AND `password` = ?");
        $stmt->bindParam(1, $username, PDO::PARAM_STR, 50);
        $stmt->bindParam(2, $password, PDO::PARAM_STR, 255);
        $stmt->execute();

        return $stmt->rowCount() > 0;
    }

    public function updateHash($username, $hash) {
        $stmt = $this->db()->prepare("UPDATE `users` SET `hash` = ? WHERE `username` = ?");
        $stmt->bindParam(1, $hash, PDO::PARAM_STR, 255);
        $stmt->bindParam(2, $username, PDO::PARAM_STR, 50);

        return $stmt->execute();
    }
}