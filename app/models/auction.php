<?php

class AuctionModel extends Bwork_Data_Eloquent
{
    protected $table = 'auctions';

    public function bids()
    {
        return $this->hasMany('BidModel', 'auction_id');
    }

    public function product()
    {
        return $this->hasOne('ProductModel', 'auction_id');
    }
}