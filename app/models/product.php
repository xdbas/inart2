<?php

class ProductModel extends Bwork_Data_Eloquent
{
    protected $table = 'products';

    public function images()
    {
        return $this->hasMany('Product_ImageModel', 'product_id');
    }

    public function auction()
    {
        return $this->hasOne('AuctionModel', 'product_id');
    }

}