<?php

class UserModel extends Bwork_Data_Eloquent
{
    protected $table = 'users';

    public static function testCapsule()
    {
        return static::where('id', '=', 1)->first();
    }

    public function buyer()
    {
        return $this->hasOne('BuyerModel', 'user_id');
    }

    public function artist()
    {
        return $this->hasOne('ArtistModel', 'user_id');
    }

    public function bids()
    {
        return $this->hasMany('BidModel', 'user_id');
    }

}