<?php

class ArtistModel extends Bwork_Data_Eloquent
{
    protected $table = 'artists';

    public function auctions()
    {
        return $this->hasMany('AuctionModel', 'artist_id');
    }
}