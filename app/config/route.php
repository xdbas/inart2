<?php

return array(
    'route' => array(
        'home/routetest' => array(
            'controller'    => 'test',
            'action'        => 'test',
            'module'        => null,
            'mockParams'    => null
        )
    )
);