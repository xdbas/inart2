<!--<textarea rows="10" cols="100" style="color: #000;">
    <?php

    /*echo $artist->name;

    echo 'AUCTIONS:' . PHP_EOL;
    foreach ($artist->auctions as $auction):

        //SKIP non active auctions???
        if ($auction->state == 0) {
            continue;
        }

        echo '-------------' . PHP_EOL;

        //--------AUCTION--------//
        echo 'id ' . $auction->id . PHP_EOL;
        echo 'minimalbid ' . $auction->minimal_bid . PHP_EOL;
        echo 'state ' . $auction->state . PHP_EOL;

        //--------BIDS--------//
        echo 'bids' . PHP_EOL;
        foreach ($auction->bids as $bid) {
            echo 'BID ' . $bid->amount . PHP_EOL;
        }

        //--------PRODUCT--------//
        echo 'product' . PHP_EOL;
        echo $auction->product->name . PHP_EOL;

        //--------PRODUCT IMAGES--------//
        foreach ($auction->product->images as $image) {
            echo $image->url . PHP_EOL;
        }
    endforeach;
*/
    ?></textarea>-->

    <div class="iframewrapper"> 
        <iframe id="wallFrame" src="<?= $this->baseUrl('artist/iframe');?>"></iframe>
        <div id="arrowLeft"></div>
        <div id="arrowRight"></div>
    </div>
    <div class="container artistDetail grid">
        <div class="row">
            <div class="col-md-2 col-sm-2 artistInfo grid-entry box">
                <img src=" <?= $this->baseUrl('img/artist/artistFoto.jpg'); ?>" />
                <div class="box box-lg animated">
                    <div class="entry-info">
                        <h4><?= $artist->name ?></h4>
                        <p>Age: 25</p>
                        <p>Graduation:  WDKA</p>
                        <p>City: Rotterdam</p>
                        <p>Hobby’s: Skating, drawing, foodies, fashion, going out, nature, basketball.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 artistInfo grid-entry">

                <div class="box box-lg animated">
                    <div class="entry-info">
                        <h4>Title</h4>
                        <p><?= $artist->description?></p>
                    </div>
                    
                </div>
            </div>
            <div class="col-md-4 col-sm-4 artistInfo artistInfogray box">

                <div class="box box-lg animated">
                    
                </div>
            </div>
            <div class="col-md-2 col-sm-2 artistInfo grid-entry">

                <div class="box box-lg animated box">
                    <div class="entry-info">
                        <h4>Contact</h4>
                        <p>hanan@info.com <br>
                        +316 23 411 234 <br>
                        www.hanan.nl</p>    
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 artistInfo artistInfogray box">

                <div class="box box-lg animated">
                     
                </div>
            </div>
            <div class="col-md-6 col-sm-6 artistInfo grid-entry">

                <div class="box box-lg animated">
                    <div class="entry-info">
                    <h4>Auction</h4> 
                    <p>AUCTION

                    Artwork name Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sed mauris elementum, blandit leo et, sollicitudin dolor. Aenean sed tristique mauris. 
                    <br><br>
                    Interdum et malesuada fames ac ante ipsum primis in faucibus. Ut et congue massa. Morbi pulvinar blandit pellentesque. Fusce velit sem, euismod ut pellentes feugiat, quis congue purus feugiat. Artwork name Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. lacinia.

                    </p>  
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                </div>
                <div class="col-md-6 col-sm-6">

                    <?php
                    $i = 0;
                    foreach ($artist->auctions as $auction):

                        if($i++ == 2) {
                            break;
                        }
                        ?>

                    <div class="col-md-6 col-sm-6">
                        <div class="box box-lg animated">
                            <div class="box-content box-service box-default">
                                <!-- Heading -->
                                 <div class="time"><h4 class="auctiontimer" data-time="<?= $auction->timer; ?>" id="auction_<?= $auction->id; ?>"></h4></div>
                                <div class="imageWrapper"><img src="<?= $this->baseUrl('img/homeAuctions/item1.jpg');?>" /></div>
                                <div class="box-container">
                    <h2><?= $auction->product->name; ?></h2>
                    <!-- Paragraph -->
                    <p><?= substr($auction->product->information, 0, 125) . '...'; ?></p>
                    
                    <!-- View Button -->
                    <div class="view-button">
                        <a href="<?= $this->baseUrl('product/view/id/' . $auction->product->id); ?>" class="btn btn-info">View auction <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div> 



<script>
    var arrowPrev = document.getElementById("arrowLeft");
    
    arrowPrev.addEventListener("click", function(e) {
        console.log(arrowPrev);
        document.getElementById('wallFrame').contentWindow.impress().prev();
    }, false);

    var arrowNext = document.getElementById("arrowRight");
    
    arrowNext.addEventListener("click", function(e) {
        console.log(arrowPrev);
        document.getElementById('wallFrame').contentWindow.impress().next();
    }, false);

    var overview = document.getElementById("overview");
    
    overview.addEventListener("click", function(e) {
        console.log(arrowPrev);
        document.getElementById('wallFrame').contentWindow.impress().goto('overview');
    }, false);

    </script>

    <script>
        $(function () {
            $('.auctiontimer').each(function() {
                
                var NowSeconds = new Date().getTime() / 1000;
                var endAuctionSeconds = $('.auctiontimer').data().time;
                var Timer = endAuctionSeconds - NowSeconds;
                $(this).countdown({until: Timer, format: 'HMS', compact: true});
            });
        });
    </script>