<div class="inner-page artists">
				<div class="container auction">
					<div class="col-md-3 col-sm-3 filter">
						<h4>Category</h4>
						<input type="checkbox" name="updated" value="no"> <span class="filterName">Comtemporay</span><br>
						<input type="checkbox" name="updated" value="no"> <span class="filterName">Modern Art</span><br>
						<input type="checkbox" name="updated" value="no"> <span class="filterName">Historical</span><br>
						<input type="checkbox" name="updated" value="no"> <span class="filterName">Sculpture</span><br>
						<input type="checkbox" name="updated" value="no"> <span class="filterName">Design</span><br>




 
					</div>
					<div class="page-mainbar column-index">
						<!-- Masonry Container -->
						<div id="container" class="grid col-md-9 col-sm-9">
							<!-- Grid Item -->
							<?php foreach ($artists as $artist): ?>
							<div class="item">
								<!-- Entry for each grid -->

								<div class="grid-entry animated">
									<img src="<?= $artist->image; ?>" />
									<!-- Grid Image Container -->
									<div class="grid-img">
										<!-- Image -->
										
										<!-- Grid Image Hover Effect -->
										<span class="grid-img-hover"></span>
										<!-- Grid Image Hover Icon -->
										<a href="img/grid-wall/wall01.jpg" class="grid-img-link">
											<i class="fa fa-search-plus hover-icon icon-left"></i>
										</a>
									</div>
									<!-- Grid entry information -->

									<div class="entry-info">
										<!-- Heading -->
										<h2><?= $artist->name; ?></h2>                                      
										<!-- Paragraph -->
										<p>
											<?php  $desc = $artist->description;
                        						 echo substr($desc, 0, 125).'...'; 
                        						 ?>
										</p>
										<!-- Share Block -->
										
										
									</div>
									<div class="view-button">
                        					<a href="<?= $this->baseUrl('artist/view/id/' . $artist->id); ?>" class="btn btn-info">View artist <i class="fa fa-angle-right"></i></a>
                    				</div>
								</div>
							</div>
							<?php endforeach ?>
							
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>