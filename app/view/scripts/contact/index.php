<div class="container grid">

<div class="row contact">
	<div class="col-md-4 col-sm-12 box">
		<div class="box box-lg animated">
	        <div class="entry-info">
			<p class="bold">Phone</p>
			<p>0031 6 123 456 78</p>

			<p>Email</p>
			<p>samplemail@email.com</p>

			<p>Addres</p>
			<p>street name 123</p>
			<p>1234 AB Amsterdam</p>
			</div>
		</div>
	</div>
	<div class="col-md-8 col-sm-8">
		<form role="form">
			<div class="form-group">
				<!-- form input -->
				<input type="text" class="form-control" id="name" placeholder="Name">
			</div>
			<div class="form-group">
				<!-- form input -->
				<input type="email" class="form-control" id="email" placeholder="Email">
			</div>
			<div class="form-group">
				<!-- form textarea -->
				<textarea class="form-control" id="comment" rows="3" placeholder="Comments"></textarea>
			</div>
			<div class="form-group">
				<!-- form Submit and reset button -->
				<button type="submit" class="btn btn-info">Submit</button>&nbsp;
				<button type="reset" class="btn btn-default">Reset</button>
			</div>
		</form>
	</div>

</div>

</div>



