
<!--
<style type="text/css">
	body, html{
		padding: 0;
		margin: 0;

	}

	iframe#wallFrame{
		width: 100%;
		height: 100%;
		border: 0;
	}

	#arrowLeft{
		position: absolute;
		top: 50%;
		margin-top: -20px;
		left: 10px;
		height: 40px;
		width: 40px;
		background: gray;
		cursor: pointer;
		background: url('<?= $this->baseUrl('assets/arrow_large_left.png');?>');
		background-repeat: no-repeat;
	}
	#arrowRight{
		position: absolute;
		top: 50%;
		margin-top: -20px;
		right: 10px;
		height: 40px;
		width: 40px;
		cursor: pointer;
		background: url('<?= $this->baseUrl('img/arrow_large_right.png');?>');
		background-repeat: no-repeat;
	}
	#arrowRight:hover, #arrowLeft:hover{
		background-position: 0 -40px;
	}

	#overview{
		position: absolute;
		top: 0;
		left:0;
		height: 40px;
		width: 100px;
		cursor: pointer;
		color: #fff;
		background-color: 	#ed5565;
		border-color: #CA3D4C;
		text-align: center;
	}

	#overview:hover{
		background-color: 	#CA3D4C;
	}

	#overview p{
			margin: 0;
			line-height: 40px;
			font-family: helvetica;
	}
	
		
</style>
	
<iframe id="wallFrame" src=""></iframe>
<div id="arrowLeft"></div>
<div id="arrowRight"></div>
<div id="overview"><p>Overzicht</p></div>
	<script>
    var arrowPrev = document.getElementById("arrowLeft");
    
    arrowPrev.addEventListener("click", function(e) {
    	console.log(arrowPrev);
    	document.getElementById('wallFrame').contentWindow.impress().prev();
	}, false);

	var arrowNext = document.getElementById("arrowRight");
    
    arrowNext.addEventListener("click", function(e) {
    	console.log(arrowPrev);
    	document.getElementById('wallFrame').contentWindow.impress().next();
	}, false);

	var overview = document.getElementById("overview");
    
    overview.addEventListener("click", function(e) {
    	console.log(arrowPrev);
    	document.getElementById('wallFrame').contentWindow.impress().goto('overview');
	}, false);

	</script>-->

	<div class="container">
		<div class="row">

			<div class="col-md-6 col-sm-6 product-info">
            <!-- Box Outer Layer [ Box 2 ] -->
            <div class="box box-lg animated">
                <div class="box-content box-service box-default">
                    <!-- Heading -->
                    <div class="imageWrapper"><img src="<?= $this->baseUrl('img/homeAuctions/image3.jpg');?>" /></div>
                    <div class="box-container">

                        <h4>Product title</h4>
                        <p>Artwork name Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sed mauris elementum, blandit leo et, sollicitudin dolor. Aenean sed tristique mauris. Sed adipiscing nisl ac nibh elementum, quis dignissim tortor ornare. Etiam luctus lectus at dictum condimentum. Quisque non aliquam nisl, eu congue turpis. Sed eleifend pharetra neque at lacinia. Maecenas ut eros imperdiet purus adipiscing interdum.</p>
                    </div>
                </div>
            </div>
        </div>   
         <div class="col-md-6 col-sm-6 product-bid">
            <!-- Box Outer Layer [ Box 2 ] -->
            <div class="box box-lg animated">
                <div class="box-content box-service box-default">
                    <!-- Heading -->
                    <div class="box-container">
                        <h4>Loop af over <span class="time-left">23:43</span> uur</h4><br>                        
                    </div>
                    <div class="col-md-6 col-sm-6">
                    	<div class="box-container">
                    		<p class="bold">Huidig bod:</p>
                    		<span class="eurosign">&euro;</span><span class="bid-amount">150</span><br><br>
                    		<p>Do you want us to inform you about the changes of this artwork?</p><br>
                    		<p>NO<input type="checkbox" name="updated" value="no"></p>
							<p>Yes keep me updated<input type="checkbox" name="updated" value="yes"></p>
                    	</div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                    	<div class="box-container">
                    		<input type="input" name="bid" class="bid-input"><br><br>
                    		<p class="bold">Biedgeschiedenis</p>
                    		<div class="bidhistory-item clearfix">
	                    		<div class="col-md-12 col-sm-12 clearfix">
	                    			<div class="history-name"><p>Sander Jansen</p></div><div class="history-bid-amount"><p>140</p></div>
	                    		</div>
	                    		<div class="col-md-12 col-sm-12 clearfix">
	                    			<div class="history-date"><p>15 april 2014</p></div><div class="history-time"><p>19:22</p></div>
	                    		</div>
                    		</div>
                    		<div class="bidhistory-item clearfix">
	                    		<div class="col-md-12 col-sm-12 clearfix">
	                    			<div class="history-name"><p>Sander Jansen</p></div><div class="history-bid-amount"><p>140</p></div>
	                    		</div>
	                    		<div class="col-md-12 col-sm-12 clearfix">
	                    			<div class="history-date"><p>15 april 2014</p></div><div class="history-time"><p>19:22</p></div>
	                    		</div>
                    		</div>
                    	</div>
                    </div>
                </div>
            </div>
        </div>   


		</div>


	</div>	
