<div class="container">
    <div class="row">

        <div class="col-md-6 col-sm-6 product-info">
            <!-- Box Outer Layer [ Box 2 ] -->
            <div class="box box-lg animated">
                <div class="box-content box-service box-default">
                    <!-- Heading -->
                    <div class="imageWrapper"><img src="<?= $images[0]; ?>" /></div>
                    <div class="box-container">

                        <h4><?php echo $auction->product->name; ?></h4>
                        <p><?php echo $auction->product->information; ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 product-bid">
            <!-- Box Outer Layer [ Box 2 ] -->
            <div class="box box-lg animated">
                <div class="box-content box-service box-default">
                    <!-- Heading -->
                    <div class="box-container">
                        <h4>Ends in <span class="auctiontimer" data-time="<?= $auction->timer; ?>" id="auction_<?= $auction->id; ?>"></span></h4><br>
                    </div>


                    <script>
                        $(function () {
                            $('.auctiontimer').each(function() {

                                var NowSeconds = new Date().getTime() / 1000;
                                var endAuctionSeconds = $('.auctiontimer').data().time;
                                var Timer = endAuctionSeconds - NowSeconds;
                                $(this).countdown({until: Timer, format: 'HMS', compact: true});
                            });
                        });
                    </script>


                    <div class="col-md-6 col-sm-6">
                        <div class="box-container">
                            <p class="bold">Highest bid:</p>
                            <span class="eurosign">&euro;</span><span class="bid-amount"><?= number_format($highest_bid, 0, '.', ','); ?></span><br><br>
                            <p>Do you want us to inform you about the changes of this artwork?</p><br>
                            <p>NO<input type="checkbox" name="updated" value="no"></p>
                            <p>Yes keep me updated<input type="checkbox" name="updated" value="yes"></p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="box-container">
                            <form method="post" action="<?= $this->baseUrl('product/addbid'); ?>">
                                <input type="hidden" name="id" value="<?= $product->id; ?>" />
                                <input type="hidden" id="auctionid" name="auction" value="<?= $auction->id; ?>" />
                                <input type="input" name="bid" class="bid-input"><br>
                                <input class="submit" type="submit" value="Place bid" /><br>
                            </form>

                            <p class="bold">Bid history</p>
                            <div class="bidhistory-item clearfix">
                                <div class="col-md-12 col-sm-12 clearfix">
                                    <div class="history-name"><p>Sander Jansen</p></div><div class="history-bid-amount"><p>140</p></div>
                                </div>
                                <div class="col-md-12 col-sm-12 clearfix">
                                    <div class="history-date"><p>15 april 2014</p></div><div class="history-time"><p>19:22</p></div>
                                </div>
                            </div>
                            <div class="bidhistory-item clearfix">
                                <div class="col-md-12 col-sm-12 clearfix">
                                    <div class="history-name"><p>Sander Jansen</p></div><div class="history-bid-amount"><p>140</p></div>
                                </div>
                                <div class="col-md-12 col-sm-12 clearfix">
                                    <div class="history-date"><p>15 april 2014</p></div><div class="history-time"><p>19:22</p></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>


</div>
