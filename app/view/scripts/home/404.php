<!DOCTYPE html>
<html>
<head>
    <!-- Title here -->
    <title>Inart, 404 Error</title>
    <!-- Description, Keywords and Author -->
    <meta name="description" content="Your description">
    <meta name="keywords" content="Your,Keywords">
    <meta name="author" content="ResponsiveWebInc">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Styles -->
    <!-- Bootstrap CSS -->
    <link href="<?= $this->baseUrl('css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Animate CSS -->
    <link href="<?= $this->baseUrl('css/animate.min.css');?>" rel="stylesheet">
    <!-- Font awesome CSS -->
    <link href="<?= $this->baseUrl('css/font-awesome.min.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= $this->baseUrl('css/style.css');?>" rel="stylesheet">
    <!--[if IE]><link rel="stylesheet" href="<?= $this->baseUrl('css/ie-style.css');?>"><![endif]-->

    <!-- Favicon -->
    <link rel="shortcut icon" href="#">
</head>

<body>

<!-- Body Wrapper -->
<div class="wrapper white">

    <!-- Inner Page Content // Start -->

    <div class="inner-page">
        <div class="container">
            <div class="page-mainbar error">
                <!-- Coming Soon Content -->
                <div class="error-content br-red">
                    <!-- Heading -->
                    <h2>Oops<span>!!!</span> it<span>'</span>s <span>404!!!</span></h2>
                    <!-- paragraph -->
                    <p>We are sorry, the page you requested cannot be found.</p>
                    <!-- Border design for page -->
                    <div class="line-border"></div>
                    <!-- Form, search button and input box -->
                    <form role="form" class="form-inline eform">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Type to search">
                        </div>
                        <button type="button" class="btn btn-info">Search</button>
                    </form>
                    <!-- Visited link list -->
                    <div class="link-list">
                        <a href="index.html">Home</a>
                        <a href="#">About us</a>
                        <a href="contactus.html">Contact</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Inner Page Content // End -->

    <!-- Footer Start -->

    <div class="footer padd-zero">
        <div class="container">
            <!-- Footer Copyright -->
            <div class="footer-copyright">
                <p>&copy; Copyright 2014 <a href="#">Inart</a></p>
            </div>
        </div>
    </div>

    <!-- Footer End -->

</div>
<!-- Footer End -->

<!-- Scroll to top -->
<span class="totop"><a href="#"><i class="fa fa-angle-up"></i></a></span>




<!-- Javascript files -->
<!-- jQuery -->
<script src="<?= $this->baseUrl('js/jquery.js');?>"></script>
<!-- Bootstrap JS -->
<script src="<?= $this->baseUrl('js/bootstrap.min.js');?>"></script>
<!-- jQuery way points -->
<script src="<?= $this->baseUrl('js/waypoints.min.js');?>"></script>
<!-- jQuery prettyPhoto & Isotope -->
<script src="<?= $this->baseUrl('js/jquery.prettyPhoto.js');?>"></script>
<!-- Respond JS for IE8 -->
<script src="<?= $this->baseUrl('js/respond.min.js');?>"></script>
<!-- HTML5 Support for IE -->
<script src="<?= $this->baseUrl('js/html5shiv.js');?>"></script>
<!-- Custom JS -->
<script src="<?= $this->baseUrl('js/custom.js');?>"></script>
<!-- This Page JavaScript -->
<script type="text/javascript" >
</script>
</body>
</html>