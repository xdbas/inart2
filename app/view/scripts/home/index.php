<!-- Slider Start -->
<div class="tp-banner-container">
<div class="tp-banner" >
<ul>
<!-- SLIDE  -->
<li data-transition="zoomout" data-slotamount="7" data-masterspeed="1000" >
    <!-- MAIN IMAGE -->
    <img src="<?= $this->baseUrl('img/slider/slide4.jpg');?>"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
    <!-- LAYERS -->
    <!-- LAYERS NR. 1 // -->
    <div class="tp-caption lfl largegreenbg br-red"
         data-x="50"
         data-y="140"
         data-speed="1500"
         data-start="1200"
         data-easing="Power4.easeOut"
         data-endspeed="300"
         data-endeasing="Linear.easeNone"
         data-captionhidden="off">Inart
    </div>
    <!-- LAYERS NR. 2 -->
    <div class="tp-caption skewfromleft medium_text paragraph"
         data-x="50"
         data-y="240"
         data-speed="800"
         data-start="1800"
         data-easing="Power4.easeOut"
         data-endspeed="400"
         data-endeasing="Power4.easeOut"
         data-captionhidden="off">At vero eos et accusamus et iusto<br />odio dignissimos ducimus qui blanditiis<br />praesentium voluptatum deleniti atque corrupti.
    </div>
    <!-- LAYERS NR. 3 // Slide Button -->
    <div class="tp-caption sfb"
         data-x="50"
         data-y="350"
         data-speed="1500"
         data-start="2600"
         data-easing="Power4.easeOut"
         data-endspeed="300"
         data-endeasing="Linear.easeNone"
         data-captionhidden="off"><a href="#" class="btn btn-info btn-lg btn-circle">Know More <i class="fa fa-angle-right"></i></a>
    </div>
</li>
<!-- SLIDE  -->
<li data-transition="zoomout" data-slotamount="7" data-masterspeed="1500" >
    <!-- MAIN IMAGE -->
    <img src="<?= $this->baseUrl('img/slider/slide4.jpg');?>"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
    <!-- LAYERS -->
    <!-- LAYERS NR. 1 // Logo Image -->
    <div class="tp-caption lfl"
         data-x="130"
         data-y="center"
         data-speed="1500"
         data-start="1200"
         data-easing="Power4.easeOut"
         data-endspeed="300"
         data-endeasing="Linear.easeNone"
         data-captionhidden="off"><img src="<?= $this->baseUrl('/img/logo.png');?>" class="img-responsive" alt="" />
    </div>
    <!-- LAYERS NR. 2 -->
    <div class="tp-caption lfr thinheadline_dark"
         data-x="500"
         data-y="120"
         data-speed="1500"
         data-start="2000"
         data-easing="Power4.easeOut"
         data-endspeed="300"
         data-endeasing="Linear.easeNone"
         data-captionhidden="off">Welcome To
    </div>
    <!-- LAYERS NR. 2.1 -->
    <div class="tp-caption lfr very_large_text"
         data-x="500"
         data-y="150"
         data-speed="1500"
         data-start="2300"
         data-easing="Power4.easeOut"
         data-endspeed="300"
         data-endeasing="Linear.easeNone"
         data-captionhidden="off">Inart
    </div>
    <!-- LAYERS NR. 3 -->
    <div class="tp-caption lfr medium_text paragraph"
         data-x="500"
         data-y="220"
         data-speed="1500"
         data-start="3000"
         data-easing="Power4.easeOut"
         data-endspeed="300"
         data-endeasing="Linear.easeNone"
         data-captionhidden="off">Art Auctions
    </div>
    <!-- LAYERS NR. 4 // Slide Button -->
    <div class="tp-caption lfr medium_text paragraph"
         data-x="500"
         data-y="250"
         data-speed="1500"
         data-start="3400"
         data-easing="Power4.easeOut"
         data-endspeed="300"
         data-endeasing="Linear.easeNone"
         data-captionhidden="off">From young artists.
    </div>
    <!-- LAYERS NR. 4.1 // Slide Button -->
    <div class="tp-caption sfb"
         data-x="500"
         data-y="325"
         data-speed="1500"
         data-start="4200"
         data-easing="Power4.easeOut"
         data-endspeed="300"
         data-endeasing="Linear.easeNone"
         data-captionhidden="off"><a href="#" class="btn btn-info btn-lg btn-circle">Join Now <i class="fa fa-angle-right"></i></a>
    </div>
</li>
</ul>
</div>
</div>
</div>

<!-- Slider End -->

<!-- Box Wrapper -->
<div class="box-wrapper">
<div class="container">
<!-- Box First Row -->
<div class="row">
    <?php
    foreach ($auctions as $auction):
    ?>
    <div class="col-md-3 col-sm-6">

        <div class="box box-lg animated">
            <div class="box-content box-service box-default">
                <!-- Heading -->
                <div class="time"><h4 class="auctiontimer" data-time="<?= $auction->timer; ?>" id="auction_<?= $auction->id; ?>"></h4></div>
                <div class="imageWrapper"><img src="<?= $this->baseUrl('img/homeAuctions/item1.jpg');?>" /></div>
                <div class="box-container">
                    <h2><?= $auction->product->name; ?></h2>
                    <!-- Paragraph -->
                    <p>
                    <?php  $desc = $auction->product->information;
                            echo substr($desc, 0, 125).'...'; 
                    ?></p>
                    
                    <!-- View Button -->
                    <div class="view-button">
                        <a href="<?= $this->baseUrl('product/view/id/' . $auction->product->id); ?>" class="btn btn-info">View auction <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    endforeach;
    ?>
</div>

    <script>
        $(function () {
            $('.auctiontimer').each(function() {
                
                var NowSeconds = new Date().getTime() / 1000;
                var endAuctionSeconds = $('.auctiontimer').data().time;
                var Timer = endAuctionSeconds - NowSeconds;
                $(this).countdown({until: Timer, format: 'HMS', compact: true});
            });
        });
    </script>
</div>
<div class="cold-md12 midStroke">
    <div class="view-button">
        <a href="<?= $this->baseUrl('auction'); ?>" class="btn btn-info">View all auctions <i class="fa fa-angle-right"></i></a>
    </div>
</div>

</div>
</div>
</div>
</div>