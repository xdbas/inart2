<div class="inner-page">
				<div class="container auction">
					<div class="page-mainbar column-index">
						<!-- Masonry Container -->
						<div id="container" class="grid">
							<!-- Grid Item -->
							<?php foreach ($auctions as $auction): ?>
							<div class="item">
								<!-- Entry for each grid -->
								<div class="grid-entry animated">
									<!-- Grid Image Container -->
									<div class="box-content box-service box-default">
						                <!-- Heading -->
						                <div class="time"><h4 class="auctiontimer" data-time="<?= $auction->timer; ?>" id="auction_<?= $auction->id; ?>"></h4></div>
						                <div class="imageWrapper">
						                	<?php

                                            //henkie
						                		$image1 = 'http://dummyimage.com/500x500/aae698/ffffff.png';
						                    	foreach($auction->product->images as $image) {
						                    		$image1 = $image->url;
						                    		break;
						                    	}
						                    ?>
						                	<img src="<?= $image1; ?>" /></div>
						                <div class="box-container">
						                    <h2><?= $auction->product->name; ?></h2>
						                    <!-- Paragraph -->
						                    <p>
						                    <?php
						                    $desc = $auction->product->information; 
                        						 echo substr($desc, 0, 125).'...'; 
                        					?></p>
						                    
						                    
						                    <!-- View Button -->
						                    <div class="view-button">
						                        <a href="<?= $this->baseUrl('product/view/id/' . $auction->product->id); ?>" class="btn btn-info">View auction <i class="fa fa-angle-right"></i></a>
						                    </div>
						                </div>
						            </div>
								</div>
							</div>
							<?php endforeach ?>
							
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>

			<script>
        $(function () {
            $('.auctiontimer').each(function() {
                
                var NowSeconds = new Date().getTime() / 1000;
                var endAuctionSeconds = $('.auctiontimer').data().time;
                var Timer = endAuctionSeconds - NowSeconds;
                $(this).countdown({until: Timer, format: 'HMS', compact: true});
            });
        });
    </script>