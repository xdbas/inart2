<!DOCTYPE html>
<html>
<head>
    <!-- Title here -->
    <title>Inart <?= $this->title; ?></title>
    <!-- Description, Keywords and Author -->
    <meta name="description" content="Your description">
    <meta name="keywords" content="Your,Keywords">
    <meta name="author" content="ResponsiveWebInc">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Styles -->
    <!-- Bootstrap CSS -->
    <link href="<?= $this->baseUrl('css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Animate CSS -->
    <link href="<?= $this->baseUrl('css/animate.min.css');?>" rel="stylesheet">
    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link href="css/settings.css" rel="stylesheet">
    <!--[if IE ]><link rel="stylesheet" href="<?= $this->baseUrl('css/settings-ie8.css');?>"><![endif]-->
    <!-- Portfolio CSS -->
    <link href="<?= $this->baseUrl('css/prettyPhoto.css');?>" rel="stylesheet">
    <!-- Countdown CSS -->
    <link href="<?= $this->baseUrl('css/jquery.countdown.css');?>" rel="stylesheet">
    <!-- Font awesome CSS -->
    <link href="<?= $this->baseUrl('css/font-awesome.min.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= $this->baseUrl('css/style.css');?>" rel="stylesheet">
    <!--[if IE]><link rel="stylesheet" href="<?= $this->baseUrl('css/ie-style.css');?>"><![endif]-->

    <?= Bwork_Helper_Asset::asset('css'); ?>

    <!-- Favicon -->
    <link rel="shortcut icon" href="#">

    <script src="<?= $this->baseUrl('js/jquery.js');?>"></script>



</head>

<body>

<!-- Body Wrapper -->
<div class="wrapper white">

<!-- Header Start -->
<div class="header">
    <!-- Header Information -->
    <div class="header-info">
        <div class="container">
            <div class="row">
                <!--<div class="col-md-4 col-sm-4">
                    <!-- Social Media -->
                    <!--<div class="social">
                        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
                        <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>-->
                <div class="col-md-4">
                    <!-- Logo -->
                    <div class="logo">
                        <!-- Heading -->
                        <a href="<?= $this->baseUrl(); ?>"><img src="<?= $this->baseUrl('img/logo.png');?>" alt="logo" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header Navigation -->
    <div class="header-navigation">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                </div>
                <div class="col-md-8">
                    <!-- Bootstrap Navbar -->
                    <nav class="navbar navbar-default" role="navigation">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle br-orange" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="<?= $this->baseUrl(); ?>">
                                        <!-- Link Icon -->
                                        
                                        <!-- Link Title -->
                                        <span class="link-title">Home</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= $this->baseUrl('auction'); ?>">
                                        <!-- Link Icon -->
                                        
                                        <!-- Link Title -->
                                        <span class="link-title">Auction</span>
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a href="<?= $this->baseUrl('artist/overview'); ?>" class="dropdown">
                                        <!-- Link Icon -->
                                    
                                        <!-- Link Title -->
                                        <span class="link-title">Artist<b class="fa"></b></span>
                                    </a>
                                    <!--<ul class="dropdown-menu dropdown-sm">
                                        <li>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="col-inner col-disable">
                                                        <h4 class="br-orange"><i class="fa fa-share heading-icon"></i> Suffered Inaltera</h4>
                                                        <p>There are many variations of box passages of Lorem Ipsum blackly available, but the majority have non suffered in alteration in some one form, by injected humour, or book randomised even slightly forms believable.</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="col-inner">
                                                        <ul class="list-unstyled">
                                                            <li><a href="<?/*= $this->baseUrl(); */?>"><i class="fa fa-arrow-right dd-link-icon"></i> 404 Error</a></li>
                                                            <li><a href="<?/*= $this->baseUrl(); */?>"><i class="fa fa-arrow-right dd-link-icon"></i> Coming Soon</a></li>
                                                            <li><a href="<?/*= $this->baseUrl(); */?>"><i class="fa fa-arrow-right dd-link-icon"></i> Features</a></li>
                                                            <li><a href="<?/*= $this->baseUrl(); */?>"><i class="fa fa-arrow-right dd-link-icon"></i> Gallery</a></li>
                                                            <li><a href="<?/*= $this->baseUrl(); */?>"><i class="fa fa-arrow-right dd-link-icon"></i> Login</a></li>
                                                            <li><a href="<?/*= $this->baseUrl(); */?>"><i class="fa fa-arrow-right dd-link-icon"></i> Register</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>-->
                                </li>
                                <li class="dropdown">
                                    <a href="<?= $this->baseUrl('about'); ?>" class="dropdown">
                                        <!-- Link Icon -->
                                        <!-- Link Title -->
                                        <span class="link-title">About<b class="fa"></b></span>
                                    </a>
                                    
                                </li>
                                <li>
                                    <a href="<?= $this->baseUrl('contact'); ?>">
                                        <!-- Link Icon -->
                                       
                                        <!-- Link Title -->
                                        <span class="link-title">Contact</span>
                                    </a>
                                </li>
                                
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Header End -->

<?= $this->content; ?>
</div>
<!-- Footer Start -->
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="footer-widget">
                    <h5>About us</h5>
                    <ul class="list-unstyled">
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Feed</a></li>
                        <li><a href="#">Blog Us</a></li>
                        <li><a href="#">Auction</a></li>
                    </ul>
                </div>
                
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="footer-widget">
                    <h5>Categories</h5>
                    <ul class="list-unstyled">
                        <li><a href="#">Painting</a></li>
                        <li><a href="#">Photography</a></li>
                        <li><a href="#">Sculpture</a></li>
                        <li><a href="#">Drawing</a></li>
                        <li><a href="#">Other</a></li>
                    </ul>
                </div>
                
            </div>
            <div class="clearfix visible-sm"></div>
            <div class="col-md-3 col-sm-6">
                <div class="footer-widget">
                    <h5>Follow us</h5>
                    <ul class="list-unstyled">
                        <li><a href="#">Facebook</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Linkedin</a></li>
                        <li><a href="#">Rss Feed</a></li>
                        <li><a href="#">Youtube</a></li>
                    </ul>
                </div>
                
            </div>
            <div class="col-md-3 col-sm-6 subscribe">
                <div class="footer-widget">
                    <h5>Newsletter</h5>
                    <input type="text" class="form-control" id="name" placeholder="email">
                    <input type="button" class="submit" value="Subscribe">
                </div>     
            </div>
        </div>
    </div>
</div>

<!-- Scroll to top -->
<span class="totop"><a href="#"><i class="fa fa-angle-up"></i></a></span>





<!-- Javascript files -->
<!-- jQuery -->

<!-- Bootstrap JS -->
<script src="<?= $this->baseUrl('js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?= $this->baseUrl('js/jquery.json.js');?>"></script>

<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script type="text/javascript" src="<?= $this->baseUrl('js/jquery.themepunch.plugins.min.js');?>"></script>
<script type="text/javascript" src="<?= $this->baseUrl('js/jquery.themepunch.revolution.min.js');?>"></script>
<!-- Masonry JS -->
<script src="<?= $this->baseUrl('js/masonry.pkgd.min.js');?>"></script>
<script src="<?= $this->baseUrl('js/imagesloaded.pkgd.min.js');?>"></script>
<!-- Cycle JS -->
<script type="text/javascript" src="<?= $this->baseUrl('js/jquery.cycle.all.js');?>"></script>
<!-- jQuery flot -->
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="<?= $this->baseUrl('js/excanvas.min.js');?>"></script><![endif]-->
<script src="<?= $this->baseUrl('js/jquery.flot.min.js');?>"></script>
<script src="<?= $this->baseUrl('js/jquery.flot.resize.min.js');?>"></script>
<!-- Count To JS  -->
<script src="<?= $this->baseUrl('js/jquery.countTo.js');?>"></script>
<!-- jQuery way points -->
<script src="<?= $this->baseUrl('js/waypoints.min.js');?>"></script>
<!-- jQuery prettyPhoto & Isotope -->
<script src="<?= $this->baseUrl('js/jquery.prettyPhoto.js');?>"></script>
<script src="<?= $this->baseUrl('js/isotope.js');?>"></script>
<!-- Jquery for Countdown  -->
<script type="text/javascript" src="<?= $this->baseUrl('js/jquery.countdown.min.js');?>"></script>
<!-- Respond JS for IE8 -->
<script src="<?= $this->baseUrl('js/respond.min.js');?>"></script>
<!-- HTML5 Support for IE -->
<script src="<?= $this->baseUrl('js/html5shiv.js');?>"></script>
<!-- Custom JS -->
<script src="<?= $this->baseUrl('js/custom.js');?>"></script>


<script src="<?= $this->baseUrl('js/jquery.plugin.js');?>"></script>
<script src="<?= $this->baseUrl('js/jscountdown.js');?>"></script>

<?= Bwork_Helper_Asset::asset('js'); ?>

<!-- This Page JavaScript -->
<script type="text/javascript" >

jQuery(function() {

                
            // initialize Masonry
                var $container = $('#container').masonry();
            // layout Masonry again after all images have loaded
                $container.imagesLoaded( function() {
                    $container.masonry();
                });
            
            // Type your codde here
    });

    // SLIDER REVOLUTION Java Script
    jQuery(document).ready(function() {
        jQuery('.tp-banner').revolution(
            {
                delay:9000,
                startheight:500,

                hideThumbs:10,

                navigationType:"none",


                hideArrowsOnMobile:"on",

                touchenabled:"on",
                onHoverStop:"on",

                navOffsetHorizontal:0,
                navOffsetVertical:20,

                stopAtSlide:-1,
                stopAfterLoops:-1,

                shadow:0,

                fullWidth:"on",
                fullScreen:"off"
            });
    });

    // Cycle Slide Js

    $('#box-slider-one').cycle({
        fx:    'uncover',
        speed:    200,
        timeout:  2500
    });

    /* ******************************************** */
    /* plot js */
    /* *************************************** */

    // $(function() {
    //     /* Chart data #1 */
    //     var d1 = [[0, 0], [1, 1.5], [2, 3.2], [3, 4.1],[4, 5.1], [5, 5], [6, 5.3], [7, 6],[8, 6.3], [9, 7], [10, 7.5], [11, 8]];

    //     var options = {
    //         series: {
    //             lines: {
    //                 show: true, fill: false, lineWidth:1
    //             },
    //             points: {
    //                 show: true, fill: true, lineWidth:2, radius:3, fillColor: "#fff"
    //             },
    //             shadowSize: 0
    //         },
    //         colors :["#fff"],
    //         grid: {
    //             hoverable: true, color: "#fff", backgroundColor:null ,borderWidth:0, borderColor:"#fff", labelMargin:10
    //         },
    //         xaxis: {
    //             ticks: 10,
    //             font: {
    //                 size: 12,
    //                 color: ["#fff"]
    //             }
    //         },
    //         yaxis: {
    //             ticks: 5,
    //             font: {
    //                 size: 12,
    //                 color: ["#fff"]
    //             }
    //         },
    //         legend: {
    //             backgroundOpacity:0,
    //             noColumns:2,
    //             labelBoxBorderColor: null
    //         }
    //     };

    //     $("<div id='tooltip'></div>").css({
    //         position: "absolute",
    //         display: "none",
    //         "border-radius":"1px",
    //         padding: "4px 5px",
    //         color:"#999",
    //         "font-size":"11px",
    //         "background-color": "#fff",
    //         "border":"1px solid #ccc",
    //         "z-index": "20"
    //     }).appendTo("body");

    //     $(".plot-chart").bind("plothover", function (event, pos, item) {
    //         if (item) {
    //             var x = item.datapoint[0].toFixed(2),
    //                 y = item.datapoint[1].toFixed(2);

    //             $("#tooltip").html(x + ", " + y)
    //                 .css({top: item.pageY+5, left: item.pageX+5})
    //                 .fadeIn(200);
    //         } else {
    //             $("#tooltip").hide();
    //         }
    //     });

    //     $.plot(".plot-chart", [ {data: d1,  label: "Revenue"} ], options);
    // });

    // Type your codde here


    
</script>
</body>
</html>