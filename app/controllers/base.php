<?php

/**
 * Class BaseController
 *
 * @property Inart_Security_User $auth
 */
abstract class BaseController extends Bwork_Controller_Action
{
    /**
     * @var Inart_Security_User $auth
     */
    public static $auth;

    public function beforeFilter()
    {
        static::$auth = new Inart_Security_User();
//        static::$auth->loginUser('iris', 'iris');
    }

    /**
     * Get the auth object
     *
     * @return Inart_Security_User
     * @throws Exception
     */
    public function auth()
    {
        if (false === static::$auth instanceof Inart_Security_User)
        {
            throw new Exception('Auth instance has not yet been initialized.');
        }

        return static::$auth;
    }

    public function display404Action()
    {
        $this->layoutEnabled = false;
        $this->getResponse()->setStatus(404);

        return new Bwork_View_Default('home/404');
    }
}