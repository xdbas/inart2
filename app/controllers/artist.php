<?php

class ArtistController extends BaseController
{

    public function display404Action()
    {
        return parent::display404Action();
    }

    public function overviewAction()
    {
        $view = new Bwork_View_Default();


        $artists = ArtistModel::all();

        $view->assignArray(
            array(
                'artists' => $artists,
            )
        );

        return $view;

    }

    public function viewAction()
    {
        $request = $this->getRequest();

        $id = $request->getArg('id', null);

        if (false === ctype_digit((string) $id)) {
            return parent::display404Action();
        }

        $artist = ArtistModel::with('auctions', 'auctions.bids', 'auctions.product', 'auctions.product.images')->where('id', '=', $id)->first();

        if (null == $artist) {
            return parent::display404Action();
        }

        $view = new Bwork_View_Default();
        $view->assignArray(
            array(
                'artist' => $artist,
            )
        );

        return $view;
    }

    public function iframeAction(){
        $this->layoutEnabled = false;
        return new Bwork_View_Default();
    }

}
