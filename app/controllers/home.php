<?php

class HomeController extends BaseController
{

    public function display404Action()
    {
        return parent::display404Action();
    }

    public function indexAction()
    {
        $auctions = AuctionModel::with('product', 'bids')->where('state', '=', 1)->limit(5)->get();

        foreach($auctions as &$auction) {

        }


        $view = new Bwork_View_Default();

        $view->assignArray(
            array(
                'auctions' => $auctions
            )
        );

        return $view;

    }

}
