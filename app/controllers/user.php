<?php

class UserController extends BaseController
{
    public function loginAction()
    {
        $this->layoutEnabled = false;

        if (true === $this->auth()->isLoggedIn()) {
            Inart_Helper::redirect($this->request->create('home/index'));
        }

        if ('POST' != $this->request->getHeader('REQUEST_METHOD', null)) {
            return new Bwork_View_Default();
        }

        $view = new Bwork_View_Default();
        $post = $this->request->post();

        if (false == isset($post['username'], $post['password'])) {
            $view->assignArray(
                array(
                    'error' => 'Please provide a username and password.'
                )
            );

            return $view;
        }

        if (false == $this->auth()->loginUser($post['username'], $post['password'])) {
            $view->assignArray(
                array(
                    'error' => 'Login failed; Incorrect login details.'
                )
            );

            return $view;
        }

        Inart_Helper::redirect($this->request->create('home/index'));
    }

    public function logoutAction()
    {
        $this->auth()->logOutUser();
        Inart_Helper::redirect($this->request->create('user/login'));
    }
}