<?php

class AuctionController extends Bwork_Controller_Action {


    public function indexAction() {

        $auctions = AuctionModel::with('product', 'product.images', 'bids')->where('state', '=', 1)->limit(5)->get();


        foreach($auctions as &$auction) {

        }

        $view = new Bwork_View_Default();

        $view->assignArray(
            array(
                'auctions' => $auctions
            )
        );

        return $view;
    }

    public function checkbidAction()
    {
        $this->layoutEnabled = false;

        $id = $_GET['id'];
        $price = $_GET['price'];

        $highest_bid = BidModel::where('auction_id', '=', $id)->orderBy('amount', 'desc')->first();

        $this->response->contentType = 'application/json';

        if($highest_bid == null
            || $highest_bid->amount == $price) {
            return json_encode(
                array('type' => false
            ));
        }

        return json_encode(array(
            'type' => true,
            'amount' => $highest_bid->amount
        ));
    }



}
