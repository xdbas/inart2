<?php

class ProductController extends BaseController
{

   /* public function beforeFilter()
    {
        parent::beforeFilter();

        $auth = $this->auth();
        if (false === $auth->isLoggedIn()) {
            Inart_Helper::redirect($this->getRequest()->create('user/login'));
            return;
        }

    }*/

    public function viewAction()
    {
        $request = $this->getRequest();

        $id = $request->getArg('id', null);

        if (false === ctype_digit((string) $id)) {
            return parent::display404Action();
        }

        $product = ProductModel::with('images')->where('id', '=', $id)->first();

        $auction = AuctionModel::with('product', 'bids', 'product.images')->where('id', '=', $product->auction_id)->first();

        $highest_bid = BidModel::where('auction_id', '=', $auction->id)->orderBy('amount', 'desc')->first();

        if (null == $product) {
            return parent::display404Action();
        }

        if($highest_bid == null) {
            $highest_bid = new StdClass();
            $highest_bid->amount = 0;
        }

        $images = array();

        foreach ($product->images as $image) {
            $images[] = $image->url;
        }

        if (count($images) == 0) {
            $images[] = 'http://dummyimage.com/500X500/aae698/ffffff.png';
        }

        $view = new Bwork_View_Default();
        $view->assignArray(
            array(
                'images' => $images,
                'product' => $product,
                'auction' => $auction,
                'highest_bid' => $highest_bid->amount,
            )
        );

        Bwork_Helper_Asset::addJS('js/auction.js');

        return $view;
    }

    public function addBidAction()
    {
        $this->layoutEnabled = false;

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $bid = new BidModel();
            $bid->amount = $_POST['bid'];
            $bid->user_id = 2;
            $bid->auction_id = $_POST['auction'];



            $highest_bid = BidModel::where('auction_id', '=', $_POST['auction'])->orderBy('amount', 'desc')->first();

            if($highest_bid != null) {
                if($highest_bid->amount >= $_POST['bid']) {
                    Inart_Helper::redirect($this->request->create('product/view/id/' . $_POST['id']));
                }
            }


            $bid->save();

            Inart_Helper::redirect($this->request->create('product/view/id/' . $_POST['id']));

        } else {
            return parent::display404Action();
        }
    }

}
